//Contains the avatar for the backend
import React from"react";
import './Body.css';
import april_avatar from '../april_avatar.png';

function AprilBody() {
 return (
      <React.Fragment>
          <div className="about-each" id="about-backend">
              <img src={april_avatar} 
              className="body-avatars" 
              alt="Avatar of a women" />
          </div>
      </React.Fragment>
    )
  }

export default AprilBody;
