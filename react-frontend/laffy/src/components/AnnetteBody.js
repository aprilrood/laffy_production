//Contains the avatar for frontend
import React from"react";
import './Body.css';
import annette_avatar from '../annette_avatar.png';

function AnnetteBody() {
 return (
      <React.Fragment>
          <div className="about-each">
              <img src={annette_avatar} 
              className="body-avatars" 
              alt="Avatar of a women" />
          </div>
      </React.Fragment>
    )
  }

export default AnnetteBody;
