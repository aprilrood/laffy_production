import React from 'react';
import axios from 'axios';


export default class Top20Box extends React.Component {

  state = {
    hashTags: []
  }
  

  componentDidMount() {
    //runs API call to get the top trending hashtags on Twitter
    axios.get('https://laffy.herokuapp.com/top20')
        .then((response) => {
          var tags = response.data[0].trends;
          var hashTags = [];
          //limits the list to only data that is using actual hashtags
          hashTags = tags.filter(tag => tag.name.includes('#'))
          //takes the top 20 in the list
          hashTags.length = 20;
          this.setState({hashTags});
        })
    }

//renders the top 20 list of hashtags with links to actual Twitter trending page
render() {
    return(
      <div className = "top20">
        {this.state.hashTags.map((tag,index) => 
          <span className = "top20Span" key={index}>
            <a href={`${tag.url}`}  target="_blank" rel="noopener noreferrer">{tag.name}</a>
          </span>
        )}
      </div>
    )
  }

}










