import React from"react";
import Dropdown from '../components/Dropdown.js';
import Logo from './Logo.js';


function Header() {
 return (
      <React.Fragment>
        <nav>
          <Dropdown />
            <div className="spacer" />
            <div className="nav-logo">
              <Logo>
              </Logo>
            </div>
            <div className="spacer" />
            <div  className="nav-links">
              <ul>
                <li>
                  <a
                    className="btn-1"
                    href="\"
                  >
                    Home
                  </a>
                </li>
                <li>
                  <a
                    className="btn-1"
                    href="about"
                  >
                    About
                  </a>
                </li>
              </ul>
            </div>
        </nav>
      </React.Fragment>
    )
  }

export default Header;
