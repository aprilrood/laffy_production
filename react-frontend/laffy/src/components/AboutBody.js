import React from"react";
import './Body.css';
import Card from '../components/Card.js';

function AboutBody() {
 return (
      <React.Fragment>
        <div className="about-team-area">
        	<div className="about-team">
        	      <div className="about-team-text"> Who's in our team? </div>
	            <Card />
	        </div>
        </div>
      </React.Fragment>
    )
  }

export default AboutBody;

