import React from 'react';
import { useSpring, animated } from 'react-spring';
import AprilBody from '../components/AprilBody.js';
import AnnetteBody from '../components/AnnetteBody.js';

const calc = (x, y) => [-(y - window.innerHeight / 2) / 20, (x - window.innerWidth / 2) / 20, 1.1]
const trans = (x, y, s) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`

function Card() {
  const [props, set] = useSpring(() => ({ xys: [0, 0, 1], config: { mass: 5, tension: 350, friction: 40 } }))
  return (
  	<React.Fragment>
  		<div className="card-area">
		    <span><animated.div
		      class="card"
		      onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
		      onMouseLeave={() => set({ xys: [0, 0, 1] })}
		      style={{ transform: props.xys.interpolate(trans) }}>
		    	<AprilBody />
		    	<div className="card-text">
		    		<h2>Backend</h2>
		    		<h4>April, the backend master.</h4>
		    	</div>
	    	</animated.div></span>
		    <span><animated.div
		      class="card"
		      onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
		      onMouseLeave={() => set({ xys: [0, 0, 1] })}
		      style={{ transform: props.xys.interpolate(trans) }}>
		    	<AnnetteBody />
		    	<div className="card-text">
		    		<h2>Frontend</h2>
		    		<h4>Annette, the frontend guru.</h4>
		    	</div>
		    </animated.div></span>
	    </div>
    </React.Fragment>
  )
}

export default Card;

