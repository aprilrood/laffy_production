import React from 'react';
import './Body.css';


function MainCTA() {
	return (
		<React.Fragment>
			<div className="cta">
				<div className="main-cta">
					<div className="main-cta-text" id="first-cta-text">
						Interested in learning how we did this? Learn more about
						twitters API.
					</div>
					<a href="https://developer.twitter.com/en/docs" target="_blank" rel="noopener noreferrer">
					<button className="button-cta" id="first-button">Learn more</button>
					</a>
				</div>
				<div className="main-cta">
					<div className="main-cta-text" id="second-cta-text">
						Have questions for us or just want to say hello?
					</div>
					<a href="contact"><button className="button-cta" id="second-button">Contact us</button></a>
				</div>
			</div>
		</React.Fragment>

	)
}

export default MainCTA;




