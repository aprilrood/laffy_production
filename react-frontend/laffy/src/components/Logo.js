
import React from 'react'; //this statement lets us use react
import logo from '../logo.png'; // importing a logo (local file)

//building a logo component
function Logo() {
	return (
		<div className="logo-area">
			<img src={logo} className="nav-logo-button" alt="logo" />
			AFFY
		</div>
	)
}

export default Logo;

