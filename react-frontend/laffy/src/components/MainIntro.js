import React from 'react';

function MainIntro() {
	return(
		<div className="intro-box">
			<div className="intro-text">
				Search through hashtags and find all your data 
				analysis through LAFFY, logistical analysis
				 facts for you.
			</div>
		</div>
	)
}

export default MainIntro;