 import React from "react";
import './Body.css';
import Slide from '../components/Slide.js';


function AboutIntro() {
 return (
      <React.Fragment>
        <div className="about-intro">
        	<Slide />
        </div> 
      </React.Fragment>
    )
  }

export default AboutIntro;

