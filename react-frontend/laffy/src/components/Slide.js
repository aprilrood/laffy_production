
import React, { useState, useCallback } from 'react';
import { useTransition, animated } from 'react-spring';

const pages = [
  //Wording and style for each slide
  ({ style }) => <animated.div style={{ ...style, background: '#EB6CAD' }}
  className="slides" id="slide1">
    <h1> Who are we? </h1>
    </animated.div>,

  ({ style }) => <animated.div style={{ ...style, background: 'white' }}  
  className="slides" id="slide2">
    <h3>Just a couple of bootcamp students
        aiming to create a simple web application 
        for all to use.</h3>
  </animated.div>,

  ({ style }) => <animated.div style={{ ...style, background: '#EB6CAD' }}  
  className="slides" id="slide3">
    <h1> Why LAFFY? </h1>
  </animated.div>,

    ({ style }) => <animated.div style={{ ...style, background: 'white' }}  
    className="slides" id="slide4">
      <h3>For our final project we wanted to create something that 
          would help people in some capacity. Seeing as there are not many
          free tools for people to use that give data on hashtags, we decided 
          to do something about that.</h3>
  </animated.div>,
]

function Slide() {
  //Declaring state variable index
  const [index, set] = useState(0)
  const onClick = useCallback(() => set(state => (state + 1) % 4), [])
  //How each page will enter and leave
  const transitions = useTransition(index, p => p, {
    from: { opacity: 0, transform: 'translate3d(100%,0,0)' },
    enter: { opacity: 1, transform: 'translate3d(0%,0,0)' },
    leave: { opacity: 0, transform: 'translate3d(-50%,0,0)' },
  })
  return (
    //When clicked return the next page.
    <div className="slides-container" onClick={onClick}>
      {transitions.map(({ item, props, key }) => {
        const Page = pages[item]
        return <Page key={key} style={props} />
      })}
    </div>
  )
}
export default Slide;
