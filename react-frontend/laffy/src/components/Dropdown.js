 //This is the dropdown menu for the mobile version.
  import React, { Component } from 'react';
  import roundedBoxes_icon from '../roundedBoxes_icon.png';
  import './Dropdown.css';


class Dropdown extends Component {
  constructor() {
    super();
    //The menu will be hidden
    this.state = {
      showMenu: false,
    };
    
    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
  }
  //When clicked the menu (dropdown) will be shown
  showMenu(event) {
    event.preventDefault();
    
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }
  
  //When clicked the menu will go away, this will also close when you click outside of the menu area
  closeMenu(event) {
    
    if (!this.dropdownMenu.contains(event.target)) {
      
      this.setState({ showMenu: false }, () => {
        document.removeEventListener('click', this.closeMenu);
      });  
      
    }
  }

  render() {
    return (
      <div>
      <img src={roundedBoxes_icon} 
        className="nav-icon-button"
        alt="Dropdown icon" 
        onClick={this.showMenu}/>
        {
          this.state.showMenu
            ? (
              <div
                className="menu"
                ref={(element) => {
                  this.dropdownMenu = element;
                }}
              >
                 <a
                    className="mobile-link"
                    href="\"
                  >
                    Home
                  </a>
                 <a
                    className="mobile-link"
                    href="about"
                  >
                    About
                  </a>
              </div>
            )
            : (
              null
            )
        }
      </div>
    );
  }
}

export default Dropdown;

