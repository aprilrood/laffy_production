import React, { Component } from 'react';
import Header from '../components/Header.js';
import Slide from '../components/Slide.js';
import AboutBody from '../components/AboutBody.js';
import Footer from '../components/Footer.js';


export default class About extends Component { 
  state = { 
  }
  
  render () {                                   
      return (
        <React.Fragment>
              <Header />
              <Slide />
              <AboutBody />
              <Footer />
         </React.Fragment>
      )
   }
}



