//React frontend file
import React from 'react';
import './App.css';
import About from './pages/About.js';
import Home from './pages/Home.js';
import Contact from './pages/Contact.js';
import {BrowserRouter, Route} from 'react-router-dom';

function App() {
  return (
     <BrowserRouter>
        <div>
          <Route exact={true} path='/' render={() => (
            <div className="App">
              <Home />
            </div>
          )}/>
          <Route exact={true} path='/about' render={() => (
            <div className="App">
              <About />
            </div>
          )}/>
          <Route exact={true} path='/contact' render={() => (
            <div className="App">
              <Contact />
            </div>
          )}/>
        </div>

      </BrowserRouter>  );
}

export default App;
