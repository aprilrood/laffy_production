
var Twitter = require('twitter');
var config = require('../config/config.js');

export const twitterFunction = (searchTerm) => {

var T = new Twitter(config);
var locationsToSend = [];
// Set up your search parameters
var params = {
  q: `${searchTerm}`,
  count: 100,
  result_type: 'recent',
  lang: 'en'
}

//Initiates search using the above parameters
T.get('search/tweets', params, function(err, data, response) {
	//if there is no error, proceed
  if(!err){
   // Loops through the returned tweets
    for(let i = 0; i < data.statuses.length; i++){
      // Get the tweet Id from the returned data
      let id = { id: data.statuses[i].id_str }
      locationsToSend.push(data.statuses[i].user.location);
   
    }
  } else {
    console.log(err);
  }
  return locationsToSend;
})
  
};
